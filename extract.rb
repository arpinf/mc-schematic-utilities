#!/usr/bin/env ruby

SCRIPT_DIRECTORY = File.expand_path(File.dirname(__FILE__))

Dir.chdir(SCRIPT_DIRECTORY)

$LOAD_PATH << "#{SCRIPT_DIRECTORY}/lib"

require 'extractor'

extractor = Extractor.new(SCRIPT_DIRECTORY)

extractor.execute
