require 'json'
require 'grabcraft'
require 'schematica'

class Extractor
  def initialize(base_directory)
    @base_directory = base_directory
    @cache_directory = "#{@base_directory}/cache"
    @models_cache_filename = "#{@cache_directory}/models_cache.json"
    @blockmaps_directory = "#{base_directory}/data/blockmaps"
    @models_directory = "#{base_directory}/models"
    @output_directory = "#{base_directory}/output"

    create_directories
    load_caches

    super()
  end

  def create_directories
    Dir.mkdir(@cache_directory) unless File.directory?(@cache_directory)
    Dir.mkdir(@models_directory) unless File.directory?(@models_directory)
    Dir.mkdir(@output_directory) unless File.directory?(@output_directory)
  end

  def load_caches
    if File.exist?(@models_cache_filename)
      @models_cache = File.open(@models_cache_filename, 'r') { |file| JSON.load(file) }
    else
      @models_cache = {}
    end
  end

  def save_caches
    File.open(@models_cache_filename, 'w') {|file| file.write(models_cache.to_json)}
  end

  def models_cache
    @models_cache
  end

  def execute
    url = ARGV.shift

    input = Input::Grabcraft.new(
      @blockmaps_directory,
      @models_directory,
      @models_cache,
      url
    )

    output = Output::Schematica.new(input)
    output.save("#{@output_directory}/#{input.model_name}.schematica")

    save_caches
  end
end
