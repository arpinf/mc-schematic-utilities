require 'nokogiri'
require 'open-uri'
require 'json'
require 'jsonpath'

module Input
  class Grabcraft
    attr :width
    attr :height
    attr :length
    attr :model_data
    attr :model_name

    def initialize(blockmaps_directory, models_directory, models_cache, url = nil)
      @blockmap_filename = "#{blockmaps_directory}/grabcraft.json"
      @blockmap = File.open(@blockmap_filename, 'r') { |file| JSON.load(file) }

      @models_directory = "#{models_directory}/grabcraft"

      Dir.mkdir(@models_directory) unless File.directory?(@models_directory)

      models_cache["grabcraft"] ||= {}
      @models_cache = models_cache["grabcraft"]

      load_model_data(url)
    end

    def load_model_data(url)
      download_blueprint(url) unless @models_cache.has_key?(url)

      render_object_id = @models_cache[url]["id"]
      model_filename = "#{@models_directory}/#{render_object_id}.json"
      download_model(url, model_filename) unless File.exist?(model_filename)

      initialize_model_data(url)
      read_model_data(model_filename)

      url =~ /.*\/minecraft\/([^\/]*)/
      grabcraft_model_name = Regexp.last_match(1).strip

      @model_name = "#{render_object_id}_#{grabcraft_model_name}"
    end

    def download_blueprint(url)
      puts "Fetching blueprint: #{url}"
      doc = Nokogiri::HTML(URI.open(url))
      puts "DONE"

      width = doc.search('td.dimension-x').first.content.to_i
      height = doc.search('td.dimension-y').first.content.to_i
      length = doc.search('td.dimension-z').first.content.to_i

      render_object_url = doc.search('//script[contains(@src,"RenderObject")]').first["src"]

      render_object_url =~ /myRenderObject_(\d+)/
      render_object_id = Regexp.last_match(1).chomp(' ').to_i

      @models_cache[url] = {
        "id" => render_object_id,
        "width" => width,
        "height" => height,
        "length" => length,
        "render_object_url" => render_object_url
      }

    end

    def download_model(url, model_filename)
      render_object_url = @models_cache[url]["render_object_url"]
      puts "Fetching model: #{render_object_url}"
      render_object_lines = URI.open(render_object_url).readlines
      puts "DONE"

      puts "Converting render data to JSON"
      render_object_json_lines = render_object_lines.collect { |line| line.delete_prefix('var myRenderObject = ')}
      render_object_json = render_object_json_lines.join("\n")

      puts "Saving model..."
      File.open(model_filename, "w") { |file| file.write(render_object_json)}
      puts "DONE"

    end

    def initialize_model_data(url)
      cache_data = @models_cache[url]
      @width = cache_data["width"]
      @height = cache_data["height"]
      @length = cache_data["length"]
      size = @width * @height * @length
      @model_data = Array.new(size, "minecraft:air/0")
    end

    def read_model_data(model_filename)
      json_render_object = File.open(model_filename, 'r') { |file| JSON.load(file) }

      validate_material(json_render_object)

      layers = json_render_object.keys.sort{|a, b| a.to_i <=> b.to_i}

      layers.each do |layer|
        y = (layer.to_i) - 1
        layer_json = json_render_object[layer]
        layer_json.each do |x_key, layer_block|
          x = (x_key.to_i) - 1
          layer_block.each do |z_key, item|
            z = (z_key.to_i) -1
            material_name = item["name"].strip
            block_index = (y * @length + z) * @width + x
            @model_data[block_index] = @blockmap[material_name]
          end
        end
      end
    end

    def validate_material(json_render_object)
      puts "Validating material list"
      unknown_material = []
      material_list_query = JsonPath.new('$..name')
      material_list_query.on(json_render_object).each do |material|
        material.strip!
        unknown_material << material unless @blockmap.has_key?(material)
      end

      unless unknown_material.empty?
        puts "Missing materials from blockmap:"
        unknown_material.uniq.sort.each do |material|
          puts "  - #{material}"
        end
        exit 100
      end
    end
  end
end
