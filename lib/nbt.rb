require 'json'


class NBTTag
  attr_accessor :name
  attr_accessor :value

  def tag_type
    nil
  end

  def initialize(name = nil, value = nil)
    self.name = name
    self.value = value
    super()
  end

  def to_hash
    {
      "tagType" => tag_type,
      "name"    => self.name,
      "value"   => self.value
    }
  end

  def to_json(options = nil)
    to_hash.to_json
  end
end

class NBTByte < NBTTag
  def tag_type
    1
  end
end

class NBTShort < NBTTag
  def tag_type
    2
  end
end

class NBTByteArray < NBTTag
  def tag_type
    7
  end
end

class NBTString < NBTTag
  def tag_type
    8
  end
end

class NBTList < NBTTag
  attr_accessor :tag_list_type
  attr_accessor :list

  def initialize(name = nil, tag_list_type = nil, list = nil)
    super(name)
    self.tag_list_type = tag_list_type
    self.list = list
  end

  def to_hash
    self.value = {
      "tagListType" => self.tag_list_type,
      "list"        => self.list
    }
    super()
  end

  def tag_type
    9
  end
end

class NBTCompound < NBTTag
  attr_accessor :list

  def initialize(name, *list)
    super(name)
    self.list = list
  end

  def to_hash
    self.value = list
    super()
  end

  def tag_type
    10
  end
end

class Schematic < NBTTag
  attr_accessor :width
  attr_accessor :height
  attr_accessor :length
  attr_accessor :blocks
  attr_accessor :data
  attr_accessor :mapping
  attr_accessor :tile_entities

  def initialize(width = nil, height = nil, length = nil, blocks = nil, data = nil, *mapping)
    self.width = width
    self.height = height
    self.length = length
    self.blocks = blocks
    self.data = data
    self.mapping = mapping
    self.tile_entities = []
  end



  def tag_type
    10
  end

  def to_hash
    {
      "tagType" => tag_type,
      "name"    => 'Schematic',
      "value"   => [
        NBTShort.new('Width', self.width),
        NBTShort.new('Height', self.height),
        NBTShort.new('Length', self.length),
        NBTString.new('Materials', 'Alpha'),
        NBTByteArray.new('Blocks', self.blocks),
        NBTByteArray.new('Data', self.data),
        NBTList.new('Entities', 0),
        NBTList.new(
          'TileEntities',
          self.tile_entities.empty? ? 0 : 10,
          self.tile_entities
        ),
        NBTCompound.new(
          'Icon',
          NBTString.new('id', 'minecraft:stone'),
          NBTByte.new('Count', 1),
          NBTShort.new('Damage', 0)
        ),
        NBTCompound.new('SchematicaMapping', *self.mapping)

      ]
    }
  end

end
