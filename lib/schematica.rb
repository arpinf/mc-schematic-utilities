require 'nbt'

module Output
  class Schematica
    attr :json_output

    def initialize(input)
      @input = input
      size = @input.model_data.length
      @blocks = []
      @data   = []
      @block_material_mapping = []
      @schematica_mapping = []
      @tile_entities = []

      verify_presence_of_required_tools

      super()
    end

    def verify_presence_of_required_tools
      unless system('nbt2json -v >/dev/null 2>&1 ')
        puts "Missing required tool 'nbt2json'"
        puts "Make sure it is in your path"
        exit 101
      end
    end

    def convert
      @input.model_data.each do |block|
        block =~ /(.*)\/(\d*)/
        block_id = Regexp.last_match(1).strip
        nbt_data = Regexp.last_match(2).strip.to_i
        @block_material_mapping << block_id unless @block_material_mapping.include?(block_id)
        @blocks << @block_material_mapping.index(block_id)
        @data << nbt_data
      end
    end

    def to_json
      convert

      width = @input.width
      length = @input.length
      height = @input.height
      schematica_mapping = @block_material_mapping.collect do |block_id|
        NBTShort.new(
            block_id,
            @block_material_mapping.index(block_id)
          )
      end

      schematic = Schematic.new(
        width, height, length,
        @blocks,
        @data,
        *schematica_mapping
      )

      {
        "nbt" => [schematic]
      }.to_json

    end

    def save(filename)
      puts "Converting via nbt2json as: #{filename}"
      command = "nbt2json -r -b -z -o #{filename}"
      IO.popen(command, "w") do |process|
        process.write self.to_json
        process.close_write
      end
    end
  end
end
